module (..., package.seeall)
----------------------------------------------------------------------------------
--
-- canvasController.lua
--
----------------------------------------------------------------------------------


--VARS
local mainCanvasGroup = display.newGroup()
local contentGroup = display.newGroup()
local startX
local xOrig
local curView
local preView
local sideNavShowing = false

local function navBarTouch(event)
    --ADD VARS TO GET ORIGIN OF GROUP BEFORE WE MOVE
    if(event.phase=="began")then
        startX=event.x
        xOrig = mainCanvasGroup.x
    elseif(event.phase=="moved")then
        
        if(xOrig + (event.x-startX)<0)then
            mainCanvasGroup.x=screen.left
        end
        
        if(xOrig + (event.x-startX)>screen.left and xOrig + (event.x-startX)<screen.right-20)then
            --MOVE GROUP THE DISTANCE FROM WHERE IS STARTED TO THE LENGTH OF THE DRAG
            mainCanvasGroup.x = xOrig + (event.x-startX)
        end
        
    elseif( event.phase == "ended") then
        --MAKE SURE WE CAN"T DRAG OFF LEFT SIDE
        if(mainCanvasGroup.x>screen.left+125)then
            transition.to( mainCanvasGroup, { time=200, x=(screen.left+125),transition=easing.outExpo  } )
            sideNavShowing=true
        else
            transition.to( mainCanvasGroup, { time=200, x=(screen.left),transition=easing.inExpo } )
            sideNavShowing=false
        end
        return true
    end
    return true
end

local function navButtonTouch(event)
    if(event.phase == 'ended')then
        
        if(sideNavShowing == false)then
            transition.to( mainCanvasGroup, { time=200, x=(screen.left+125),transition=easing.outExpo } )
            sideNavShowing=true
        else
            transition.to( mainCanvasGroup, { time=200, x=(screen.left),transition=easing.inExpo } )
            sideNavShowing=false
        end 
    end
    return true
end

function init(group)
    --ADD SHADOW
    local shadow = display.newImageRect("shadow.png", 35, screen.bottom-screen.top)
    shadow:setReferencePoint(display.TopLeftReferencePoint)
    shadow.x=mainCanvasGroup.x-35
    shadow.y=mainCanvasGroup.y
    mainCanvasGroup:insert(shadow)
    
    
    --ADD BKG 
    local canvasBkg = display.newRect(mainCanvasGroup, mainCanvasGroup.x, mainCanvasGroup.y, screen.right-screen.left, screen.bottom-screen.top)
    canvasBkg:setFillColor(211,209,209)
    
    --ADD CONTENT GROUP
    mainCanvasGroup:insert(contentGroup)
    
    --ADD NAV BAR
    local navBkg = display.newRect(mainCanvasGroup, mainCanvasGroup.x, mainCanvasGroup.y, screen.right-screen.left, 50)
    navBkg:setFillColor(0,73,117)
    navBkg:addEventListener( "touch", navBarTouch )
    
    --ADD NAV BAR BUTTON
    local navButton = display.newRect(mainCanvasGroup, mainCanvasGroup.x+5, mainCanvasGroup.y+3, 44, 44)
    navButton:setFillColor(0,146,234)
    navButton:addEventListener( "touch", navButtonTouch )
    
    --ADD TO GROUP
    group:insert(mainCanvasGroup)
    mainCanvasGroup.x=screen.right
end

local function animInView()
    transition.to(mainCanvasGroup,{time=200, x=screen.left})
end

local function switchContent()
    if(preView~=nil)then
        preView.destroyView()
    end
    curView.loadView(contentGroup)
    timer.performWithDelay(100, animInView, 1)
    --animInView()
end

local function animOutView()
    transition.to(mainCanvasGroup,{time=200, x=screen.right,onComplete=switchContent})
end

function loadView(view)
    print('CanvasController loading view...')
    if(curView~=view)then
        preView=curView
        curView=view
        animOutView() 
    end
end
