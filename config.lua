
if system.getInfo("platformName") == "Android" then
    application =
    {
        content =
        {
            --zoom
            width = 320,
            height = 480,
            fps = 60,
            scale = "zoomEven",
            imageSuffix = {
                ["@2x"] = 2,
		["@15x"] = 1.5,
            },
        },
    }
elseif system.getInfo("model") == "iPad" then
    
    application =
    {
        content =
        {
            --zoom
            width = 320,
            height = 480,
            fps = 60,
            scale = "zoomEven",
            imageSuffix = {
                ["@2x"] = 2,
		["@15x"] = 1.5,
            },
        },
    }
elseif ( system.getInfo( "model" ) == 'iPhone' ) and ( display.pixelHeight > 960 ) then
         application =
    {
	content =
	{
            --zoom
            width = 320,
            height = 568,
            fps = 60,
            --scale = "zoomEven",
            scale = "letterbox",
            imageSuffix = {
                ["@2x"] = 2,
                ["@15x"] = 1.5,
            },
	},
    }  
else
    application =
    {
	content =
	{
            --zoom
            width = 320,
            height = 480,
            fps = 60,
            --scale = "zoomEven",
            scale = "letterbox",
            imageSuffix = {
                ["@2x"] = 2,
                ["@15x"] = 1.5,
            },
	},
    }	
end

